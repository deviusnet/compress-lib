----------------------------------------------------------------------------------
-- LuaBit v1.0
-- Bitwise Operation Library for Lua
--
-- It's based on the library created by hanzhao but updated to use a more modern Lua code. Original library is available
-- at: http://luaforge.net/projects/bit/
-- Please note that bit.brshift and bit.blshift only support numbers within 32 bits. Two utility functions are provided
-- as well: bit.tobits(n) which converts n into a bit table(which is a 1/0 sequence) with high bits first, and
-- bit.tonumb(bit_tbl), which converts a bit table into a number.
--
-- @usage
--   bit.bnot(n) - bitwise not (~n)
--   bit.band(m, n) - bitwise and (m & n)
--   bit.bor(m, n) - bitwise or (m | n)
--   bit.bxor(m, n) - bitwise xor (m ^ n)
--   bit.brshift(n, bits) - right shift (n >> bits)
--   bit.blshift(n, bits) - left shift (n << bits)
--   bit.blogic_rshift(n, bits) - logic right shift(zero fill >>>)
--
-- @release 1.0
-- @Author hanzhao <abrash_han@hotmail.com>
-- @author Ricardo Graça <ricardo@devius.net>
-- @copyright 2012 Ricardo Graça
-- @copyright 2006~2007 hanzhao
----------------------------------------------------------------------------------
-- The MIT License (MIT)
-- Copyright (c) <year> <copyright holders>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
-- rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
-- Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
-- OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
----------------------------------------------------------------------------------

-- Function declarations
local check_int
local to_bits
local tbl_to_number
local expand
local bit_or
local bit_and
local bit_not
local bit_xor
local bit_rshift
local bit_logic_rshift
local bit_lshift
local bit_xor2

function check_int(n)
	-- checking not float
	if (n - math.floor(n) > 0) then
		error("trying to use bitwise operation on non-integer!")
	end
end

function to_bits(n)
	check_int(n)
	if (n < 0) then
		-- negative
		return to_bits(bit_not(math.abs(n)) + 1)
	end
	-- to bits table
	local tbl = {}
	local cnt = 1
	while (n > 0) do
		local last = math.mod(n, 2)
		if (last == 1) then
			tbl[cnt] = 1
		else
			tbl[cnt] = 0
		end
		n = (n - last) / 2
		cnt = cnt + 1
	end

	return tbl
end

function tbl_to_number(tbl)
	local rslt = 0
	local power = 1

	for i = 1, #tbl do
		rslt = rslt + tbl[i] * power
		power = power * 2
	end

	return rslt
end

local function expand(tbl_m, tbl_n)
	local big = {}
	local small = {}
	if (#tbl_m > #tbl_n) then
		big = tbl_m
		small = tbl_n
	else
		big = tbl_n
		small = tbl_m
	end
	-- expand small
	for i = #small + 1, #big do
		small[i] = 0
	end
end

local function bit_or(m, n)
	local tbl_m = to_bits(m)
	local tbl_n = to_bits(n)
	expand(tbl_m, tbl_n)

	local tbl = {}
	local rslt = math.max(#tbl_m, #tbl_n)
	for i = 1, rslt do
		if (tbl_m[i] == 0 and tbl_n[i] == 0) then
			tbl[i] = 0
		else
			tbl[i] = 1
		end
	end

	return tbl_to_number(tbl)
end

local function bit_and(m, n)
	local tbl_m = to_bits(m)
	local tbl_n = to_bits(n)
	expand(tbl_m, tbl_n)

	local tbl = {}
	local rslt = math.max(#tbl_m, #tbl_n)
	for i = 1, rslt do
		if (tbl_m[i] == 0 or tbl_n[i] == 0) then
			tbl[i] = 0
		else
			tbl[i] = 1
		end
	end

	return tbl_to_number(tbl)
end

local function bit_not(n)
	local tbl = to_bits(n)
	local size = math.max(#tbl, 32)

	for i = 1, size do
		if (tbl[i] == 1) then
			tbl[i] = 0
		else
			tbl[i] = 1
		end
	end

	return tbl_to_number(tbl)
end

local function bit_xor(m, n)
	local tbl_m = to_bits(m)
	local tbl_n = to_bits(n)
	expand(tbl_m, tbl_n)

	local tbl = {}
	local rslt = math.max(#tbl_m, #tbl_n)
	for i = 1, rslt do
		if (tbl_m[i] ~= tbl_n[i]) then
			tbl[i] = 1
		else
			tbl[i] = 0
		end
	end

	return tbl_to_number(tbl)
end

local function bit_rshift(n, bits)
	check_int(n)

	local high_bit = 0
	if (n < 0) then
		-- negative
		n = bit_not(math.abs(n)) + 1
		high_bit = 2147483648 -- 0x80000000
	end

	for i = 1, bits do
		n = n / 2
		n = bit_or(math.floor(n), high_bit)
	end
	return math.floor(n)
end

-- logic rightshift assures zero filling shift
local function bit_logic_rshift(n, bits)
	check_int(n)
	if (n < 0) then
		-- negative
		n = bit_not(math.abs(n)) + 1
	end
	for i = 1, bits do
		n = n / 2
	end
	return math.floor(n)
end

local function bit_lshift(n, bits)
	check_int(n)

	if (n < 0) then
		-- negative
		n = bit_not(math.abs(n)) + 1
	end

	for i = 1, bits do
		n = n * 2
	end
	return bit_and(n, 4294967295) -- 0xFFFFFFFF
end

local function bit_xor2(m, n)
	local rhs = bit_or(bit_not(m), bit_not(n))
	local lhs = bit_or(m, n)
	local rslt = bit_and(lhs, rhs)
	return rslt
end

--------------------
-- bit lib interface

return {
	-- bit operations
	bnot = bit_not,
	band = bit_and,
	bor = bit_or,
	bxor = bit_xor,
	rshift = bit_rshift,
	lshift = bit_lshift,
	bxor2 = bit_xor2,
	blogic_rshift = bit_logic_rshift,

	-- utility func
	tobits = to_bits,
	tonumb = tbl_to_number,
}
